#include <QApplication>
#include <QDebug>

#include "pamhandle.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    auto pm = new AuthenticateUserJob("passwd", "david");

    QObject::connect(pm, &PamJob::prompt, &app, [&pm](const QString &prompt) {
        qDebug() << "prompt:" << prompt;
        if (prompt == "Current password: ") {
            pm->respond("MY REAL PASSWORD HERE");
        } else {
            pm->respond("foo");
        }
    });

    QObject::connect(pm, &PamJob::finished, &app, [&app](bool success) {
        qDebug() << "Finished:" << success;
        qApp->quit();
    });


    pm->start();

    return app.exec();
}

